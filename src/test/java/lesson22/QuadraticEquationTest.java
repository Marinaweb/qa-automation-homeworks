package lesson22;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class QuadraticEquationTest {
    private QuadraticEquation quadraticEquation;
    @Test (dataProvider = "coefficientsForEquationWithNoRoots")
    public void testGetWithoutRoots(double coefficient1, double coefficient2, double coefficient3){
        int expectedNumberOfRoots = 0;

        quadraticEquation = new QuadraticEquation(coefficient1, coefficient2, coefficient3);
        double discriminant = quadraticEquation.countDiscriminant();
        int actualNumberOfRoots = quadraticEquation.numberOfRoots(discriminant);

        Assert.assertEquals(actualNumberOfRoots, expectedNumberOfRoots);
    }

    @Test (dataProvider = "coefficientsForEquationWithOneRoots")
    public void testGetWithOneRoot(double coefficient1, double coefficient2, double coefficient3, double expectedRoot){

        quadraticEquation = new QuadraticEquation(coefficient1, coefficient2, coefficient3);
        double discriminant = quadraticEquation.countDiscriminant();
        double actualRoot = quadraticEquation.countOneRoot(discriminant);

        Assert.assertEquals(actualRoot, expectedRoot, 0.0001);
    }

    @Test (dataProvider = "coefficientsForEquationWithTwoRoots")
    public void testGetWithTwoRoots(double coefficient1, double coefficient2, double coefficient3,
                                    double expectedFirstRoot, double expectedSecondRoot){

        quadraticEquation = new QuadraticEquation(coefficient1, coefficient2, coefficient3);
        double discriminant = quadraticEquation.countDiscriminant();
        double [] actualRoots = quadraticEquation.countTwoRoots(discriminant);

        Assert.assertEquals(actualRoots[0], expectedFirstRoot, 0.0001);
        Assert.assertEquals(actualRoots[1], expectedSecondRoot, 0.0001);
    }

    @DataProvider
    private Object[][] coefficientsForEquationWithNoRoots () {
        return new Object[][]{
                {7, 2, 4},
                {5, 5, 5},
                {2, 2, 1}
        };
    }

    @DataProvider
    private Object[][] coefficientsForEquationWithOneRoots () {
        return new Object[][]{
                {4, 4, 1, -0.5}
        };
    }

    @DataProvider
    private Object[][] coefficientsForEquationWithTwoRoots () {
        return new Object[][]{
                {2, 3, 1, -0.5, -1.0},
                {1, 5, 6, -2, -3}
        };
    }
}