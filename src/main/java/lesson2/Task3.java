package lesson2;

public class Task3 {
    final static int fullWeeksPerMounth = 4;
    final static int workDaysPerWeek = 5;
    final static int daysPerWeek = 7;

    public static void main(String[] args) {
        int numberOfMonth = Integer.parseInt(args[0]);
        double hourlyCost = Double.parseDouble(args[1]);
        double taxPercentage = Double.parseDouble(args[2]);

        double salary = 0;
        double salaryWithTaxes = 0;
        double salaryInALeapYear = 0;
        double salaryInALeapYearWithTaxes = 0;

        if(numberOfMonth >= 1 && numberOfMonth <= 12 && numberOfMonth != 2) {
            salary = numberOfWorkDays(numberOfMonth) * 8 * hourlyCost;
            salaryWithTaxes = salary * (1 + taxPercentage / 100);
        } else if (numberOfMonth == 2) {
            salary = numberOfWorkDays(numberOfMonth) * 8 * hourlyCost;
            salaryWithTaxes = salary * (1 + taxPercentage / 100);
            salaryInALeapYear = (numberOfWorkDays(numberOfMonth) + 1) * 8 * hourlyCost;
            salaryInALeapYearWithTaxes = salaryInALeapYear* (1 + taxPercentage / 100);
        }

        if(numberOfMonth != 2) {
            System.out.println("Salary in " + numberOfMonth + " month is " + salary);
            System.out.println("Salary with taxes in " + numberOfMonth + " month is " + salaryWithTaxes);
        } else {
            System.out.println("Salary in " + numberOfMonth + " month is " + salary);
            System.out.println("Salary with taxes in " + numberOfMonth + " month is " + salaryWithTaxes);
            System.out.println("\nSalary in leap year in " + numberOfMonth + " month is " + salaryInALeapYear);
            System.out.println("Salary with taxes in a leap year in " + numberOfMonth + " month is " +
                    salaryInALeapYearWithTaxes);
        }
    }
    public static int numberOfWorkDays (int numberOfMonth) {
        int[] numberOfDaysPerMonth = new int[]{31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
        int numberOfWorkDays = fullWeeksPerMounth * workDaysPerWeek +
                (numberOfDaysPerMonth[numberOfMonth - 1] - daysPerWeek * fullWeeksPerMounth);
        return numberOfWorkDays;
    }
}
