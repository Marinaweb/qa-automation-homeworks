package lesson2;

import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter your number");
        int number = scanner.nextInt();

        String oddOrEven = "odd";
        if (number%2 == 0) {
            oddOrEven = "even";
        }

        String positiveOrNegative = "number is not positive or negative";
        if (number > 0) {
            positiveOrNegative = "positive";
        } else if (number < 0) {
            positiveOrNegative = "negative";
        }

        String primeOrComposite = number > 0 ? "prime" : "composite";
        for (int i = 2; i < number; i++) {
            if (number%i == 0){
                primeOrComposite = "composite";
                break;
            }
        }

        System.out.println("Your number is: \n" + oddOrEven + "\n" + positiveOrNegative + "\n" + primeOrComposite);

    }
}
