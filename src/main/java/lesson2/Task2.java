package lesson2;

import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        double pricePerGood = Double.parseDouble(args[0]);

        Scanner scanner = new Scanner(System.in);
        System.out.println("How many goods would your like to buy?");
        int quantityOfGoods = scanner.nextInt();

        if(quantityOfGoods <= 0) {
            System.out.println("Wrong quantity of goods. It should be more than 0.");
        } else {
            System.out.println("Total cost of goods is " + totalCostOfGoods(pricePerGood, quantityOfGoods));
        }
    }

    public static double totalCostOfGoods(double pricePerGood, int quantityOfGoods){
        double discount = 0;
        if(quantityOfGoods <= 0) {
            System.out.println("Wrong quantity of goods. It should be more than 0.");
        } else if(quantityOfGoods < 10){
            discount = 0;
        } else if(quantityOfGoods >= 10 && quantityOfGoods < 20) {
            discount = 5;
        } else if(quantityOfGoods >= 20 && quantityOfGoods < 30){
            discount = 10;
        } else if(quantityOfGoods >= 30 && quantityOfGoods < 80){
            discount = 12 + (quantityOfGoods / 10 - 3) * 0.4;
        } else {
            discount = 13;
        }

        double totalCostOfGoods = pricePerGood * quantityOfGoods * (1 - discount / 100);
        return totalCostOfGoods;
    }
}
