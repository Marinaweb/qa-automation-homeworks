package lesson1;

public class Task2 {
    static final int numberOfMonthsPerYear = 12;
    static final int numberOfWeeksPerMonth = 4;
    public static void salaryCounter(double numberOfWorkingHoursPerWeek, double hourlyCost, double taxPercentage) {
        double convertedTaxPercentage = 1 + taxPercentage / 100;

        System.out.println("Monthly salary without taxes: " + numberOfWorkingHoursPerWeek * hourlyCost *
                numberOfWeeksPerMonth);

        System.out.println("Monthly salary with taxes: " + numberOfWorkingHoursPerWeek * hourlyCost *
                numberOfWeeksPerMonth * convertedTaxPercentage);

        System.out.println("Salary per year without taxes: " + numberOfWorkingHoursPerWeek * hourlyCost *
                numberOfWeeksPerMonth * numberOfMonthsPerYear);

        System.out.println("Salary per year with taxes: " + numberOfWorkingHoursPerWeek * hourlyCost *
                numberOfWeeksPerMonth * numberOfMonthsPerYear * convertedTaxPercentage);
    }

    public static void main(String[] args) {
        salaryCounter(40,5.5,22.3);
    }
}
