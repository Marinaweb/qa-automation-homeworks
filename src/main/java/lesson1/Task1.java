package lesson1;

public class Task1 {
    public static void raisingANumberToAPower(double x, double y) {
        System.out.println("Power of x and y is: " + Math.pow(x,y));
    }

    public static void main(String[] args) {
        raisingANumberToAPower(2,3);
    }
}
