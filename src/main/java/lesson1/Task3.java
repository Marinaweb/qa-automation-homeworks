package lesson1;

import java.util.Scanner;

public class Task3 {
    static final int numberOfMonthsPerYear = 12;
    static final int numberOfWeeksPerMonth = 4;
    public static void calculateHourlyCost() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the number of working hours per week");
        double numberOfWorkingHoursPerWeek = scanner.nextDouble();
        System.out.println("Enter annual salary with taxes");
        double annualSalaryWithTaxes = scanner.nextDouble();
        System.out.println("Enter tax percentage");
        double taxPercentage = scanner.nextDouble();

        double hourlyCost = (annualSalaryWithTaxes * (1 - taxPercentage / 100)) /
                (numberOfWorkingHoursPerWeek * numberOfWeeksPerMonth * numberOfMonthsPerYear);
        System.out.println("At " + numberOfWorkingHoursPerWeek + " working hours per week with an annual salary of "
                + annualSalaryWithTaxes + " and tax of " + taxPercentage + "% an hour of time costs " + hourlyCost);
    }

    public static void main(String[] args) {
        calculateHourlyCost();
    }
}
