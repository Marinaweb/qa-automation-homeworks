package lesson3;

import java.util.Arrays;
import java.util.Comparator;

public class Task2 {
    public static void main(String[] args) {
        Integer[] initialArray = new Integer[args.length];
        for (int i = 0; i < args.length; i++) {
            initialArray[i] = Integer.parseInt(args[i]);
        }
        System.out.println(Arrays.toString(initialArray));

        Arrays.sort(initialArray);
        System.out.println(Arrays.toString(initialArray));

        Arrays.sort(initialArray, Comparator.reverseOrder());
        System.out.println(Arrays.toString(initialArray));

        innerSort(initialArray);
    }
    public static void innerSort(Integer[] initialArray){
        Integer [] innerSortedArray = new Integer[initialArray.length];

        for (int i = 0; i < initialArray.length; i++) {
            if(i%2 == 0) {
                innerSortedArray[i/2] = initialArray[i];
                continue;
            }
            innerSortedArray[initialArray.length - 1 - i/2] = initialArray[i];
        }

        System.out.println(Arrays.toString(innerSortedArray));
    }
}
