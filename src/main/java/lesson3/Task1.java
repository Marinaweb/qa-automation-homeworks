package lesson3;

import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        drawFigure();

        String answer = "y";
        Scanner scanner = new Scanner(System.in);

        do {
            System.out.println("Would you like to draw one more figure");
            answer = scanner.next();
            if(answer.equalsIgnoreCase("y")) drawFigure();
        } while(answer.equalsIgnoreCase("y"));
    }
    public static void drawFigure() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Which figure would you like to draw? You could select from list: \n- rectangle - enter 1\n" +
                "- triangle - enter 2\n- right triangle - enter 3\n- inverse right triangle - enter 4\n" +
                "Please specify your choice:");
        int figure = scanner.nextInt();

        switch(figure) {
            case 1:
                drawRectangle();
                break;
            case 2:
                drawTriangle();
                break;
            case 3:
                drawRightTriangle();
                break;
            case 4:
                drawInverseRightTriangle();
                break;
            default:
                System.out.println("Something went wrong");
                break;
        }
    }

    public static void drawRectangle () {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Specify the height of the rectangle");
        int height = scanner.nextInt();
        System.out.println("Specify the width of the rectangle");
        int width = scanner.nextInt();

        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                System.out.print("*");
            }
            System.out.print("\n");
        }
    }

    public static void drawTriangle(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Specify the height of the triangle");
        int height = scanner.nextInt();

        int numberOfStars = height * 2 - 1;
        int numberOfSpaces = numberOfStars / 2;
        int middle = 1;


        for (int i = 0; i < height; i++) {
            for (int j = 0; j < numberOfStars; j++) {
                if(j <= numberOfSpaces - 1){
                    System.out.print(" ");
                } else if(j > numberOfSpaces - 1 && j <= numberOfSpaces + middle - 1) {
                    System.out.print("*");
                }
            }

            System.out.print("\n");
            numberOfSpaces--;
            middle+=2;
        }
    }

    public static void drawRightTriangle() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Specify the height of the triangle");
        int height = scanner.nextInt();

        int numberOfStars = 1;

        for (int i = 0; i < height; i++) {
            for (int j = 0; j < height; j++) {
                if(j <= numberOfStars - 1){
                    System.out.print("*");
                }
            }

            System.out.print("\n");
            numberOfStars++;
        }
    }

    public static void drawInverseRightTriangle() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Specify the height of the triangle");
        int height = scanner.nextInt();

        int numberOfSpaces = height - 1;

        for (int i = 0; i < height; i++) {
            for (int j = 0; j < height; j++) {
                if(j <= numberOfSpaces - 1) {
                    System.out.print(" ");
                } else {
                    System.out.print("*");
                }
            }

            System.out.print("\n");
            numberOfSpaces--;
        }
    }
}
