package lesson3;

import java.util.Arrays;

public class Task4 {
    final static int fullWeeksPerMonth = 4;
    final static int workDaysPerWeek = 5;
    final static int daysPerWeek = 7;
    final static int[] numberOfDaysPerMonth = new int[]{31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

    final static  String[] months = new String[] {"JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"};

    public static void main(String[] args) {
        double hourlyCost = Double.parseDouble(args[0]);
        double taxPercentage = Double.parseDouble(args[1]);

        int[] numberOfWorkingDays = numberOfWorkDaysPerMonth();

        double[] salaryPerMonth = salaryPerEachMonth(numberOfWorkingDays, hourlyCost);
        double[] salaryPerMonthWithTaxes = salaryPerEachMonthWithTaxes(salaryPerMonth, taxPercentage);

        printSalary(salaryPerMonth, salaryPerMonthWithTaxes);
    }

    public static void printSalary(double[] salaryPerMonth, double[] salaryPerMonthWithTaxes) {
        for (int i = 0; i < salaryPerMonth.length; i++) {
            System.out.println(months[i] + " " + salaryPerMonth[i] + " " + salaryPerMonthWithTaxes[i] + "\n");
        }

        double yearSalary = Arrays.stream(salaryPerMonth).sum();
        double yearSalaryWithTaxes = Arrays.stream(salaryPerMonthWithTaxes).sum();

        System.out.println("TOTAL " + yearSalary + " " + yearSalaryWithTaxes);
    }
    public static double[] salaryPerEachMonthWithTaxes(double[] salaryPerMonth, double taxPercentage){
        double[] salaryPerMonthWithTaxes = new double[12];

        for (int i = 0; i < salaryPerMonthWithTaxes.length; i++) {
            salaryPerMonthWithTaxes[i] = salaryPerMonth[i] * (1 + taxPercentage / 100);
        }

        return salaryPerMonthWithTaxes;
    }
    public static double[] salaryPerEachMonth(int[] numberOfWorkingDays, double hourlyCost) {
        double[] salaryPerMonth = new double[12];

        for (int i = 0; i < salaryPerMonth.length; i++) {
            salaryPerMonth[i] = numberOfWorkingDays[i] * 8 * hourlyCost;
        }

        return salaryPerMonth;
    }
    public static int[] numberOfWorkDaysPerMonth () {
        int[] numberOfWorkingDaysPerMonth = new int[12];
        int restOfDays = 0;

        for (int i = 0; i < numberOfWorkingDaysPerMonth.length; i++) {
            if(i == 0) {
                restOfDays = numberOfDaysPerMonth[i] - fullWeeksPerMonth * daysPerWeek;
                numberOfWorkingDaysPerMonth[i] = fullWeeksPerMonth * workDaysPerWeek + restOfDays;
                continue;
            }

            int firstWeek;
            int workingDaysOfFirstWeek;
            int hollydays = 2;

            if(restOfDays <= 5){
                workingDaysOfFirstWeek = workDaysPerWeek - restOfDays;
                firstWeek = workingDaysOfFirstWeek + hollydays;
            } else {
                workingDaysOfFirstWeek = 0;
                firstWeek = 1;
            }

            int fullWeeks = 0;
            int numberOfDaysForMonth = numberOfDaysPerMonth[i] - firstWeek;
            while(numberOfDaysForMonth >= 7) {
                numberOfDaysForMonth = numberOfDaysForMonth - 7;
                fullWeeks++;
            }

            restOfDays = numberOfDaysForMonth;

            if(restOfDays == 6) {
                numberOfWorkingDaysPerMonth[i] = workingDaysOfFirstWeek + fullWeeks * workDaysPerWeek + restOfDays - 1;
                continue;
            }

            numberOfWorkingDaysPerMonth[i] = workingDaysOfFirstWeek + fullWeeks * workDaysPerWeek + restOfDays;
        }

        return numberOfWorkingDaysPerMonth;
    }
}
