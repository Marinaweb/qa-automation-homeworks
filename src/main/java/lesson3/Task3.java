package lesson3;

import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please, enter total amount of the credit");
        double sumOfCredit = scanner.nextDouble();
        System.out.println("Please, enter monthly percentage");
        double monthlyPercentage = scanner.nextDouble();

        System.out.println("If your would like to know number of months for close credit - specify 1\n" +
                "If your would like to know monthly payment - specify 2\n" +
                "Please make your choice: ");
        int choice = scanner.nextInt();

        if (choice == 1) {
            System.out.println("Please, enter your monthly payment");
            double monthlyPayment = scanner.nextDouble();

            countNumberOfMonthlyPayments(monthlyPayment, sumOfCredit, monthlyPercentage);
        } else if (choice == 2) {
            System.out.println("Please, enter number of month would you like to pay for the credit");
            int numberOfMonth = scanner.nextInt();

            countMonthlyPayment(numberOfMonth, sumOfCredit, monthlyPercentage);
        } else {
            System.out.println("Entered choice is not correct");
        }
    }

    public static void countNumberOfMonthlyPayments (double monthlyPayment, double sumOfCredit,double monthlyPercentage) {
        int numberOfMonthlyPayments = 0;

        while(sumOfCredit > 0){
            sumOfCredit = (sumOfCredit - monthlyPayment) + sumOfCredit * monthlyPercentage / 100;
            numberOfMonthlyPayments++;
        }

        System.out.println("Your credit would be closed in " + numberOfMonthlyPayments + " months.");
    }

    public static void countMonthlyPayment(int numberOfMounthlyPayments, double sumOfCredit,double monthlyPercentage) {
        double monthlyPayment = (sumOfCredit / numberOfMounthlyPayments) * (1 + monthlyPercentage / 100);

        System.out.println("Your monthly payment is " + monthlyPayment);
    }
}
