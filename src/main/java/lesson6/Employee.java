package lesson6;

public class Employee {
    private String firstName;
    private String lastName;
    private String position;
    private double hourlySalary;

    public Employee(String firstName, String lastName, double hourlySalary) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.hourlySalary = hourlySalary;
    }

    public Employee(String firstName, String lastName, String position, double hourlySalary) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.position = position;
        this.hourlySalary = hourlySalary;
    }

    protected double salaryPerMonth(double hourlySalary, int numberOfWorkingDays){
      return hourlySalary * numberOfWorkingDays;
    }

    protected double salaryPerMonthWithTaxes(double hourlySalary, int numberOfWorkingDays, double taxPercentage) {
        return hourlySalary * numberOfWorkingDays * (1 + taxPercentage / 100);
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public double getHourlySalary() {
        return hourlySalary;
    }

    public void setHourlySalary(double hourlySalary) {
        this.hourlySalary = hourlySalary;
    }
}
