package lesson6;

public class Credit {
    private int amountOfLoan;
    private int lendingPeriod;
    private double lendingRate;

    public Credit() {
    }

    public Credit(int amountOfLoan, int lendingPeriod, double lendingRate) {
        this.amountOfLoan = amountOfLoan;
        this.lendingPeriod = lendingPeriod;
        this.lendingRate = lendingRate;
    }

    protected double monthPayment(int amountOfLoan, int lendingPeriod, double lendingRate) {
        return amountOfLoan / lendingPeriod * (1 + lendingRate / 100);
    }

    protected double bankInterest(int amountOfLoan, double lendingRate) {
        return amountOfLoan * (lendingRate / 100);
    }

    public int getAmountOfLoan() {
        return amountOfLoan;
    }

    public int getLendingPeriod() {
        return lendingPeriod;
    }

    public double getLendingRate() {
        return lendingRate;
    }
}
