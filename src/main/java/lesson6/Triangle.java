package lesson6;

public class Triangle {
    private double side1;
    private double side2;
    private double side3;

    public Triangle(double side1, double side2, double side3) {
        this.side1 = side1;
        this.side2 = side2;
        this.side3 = side3;
    }

    public double getSide1() {
        return side1;
    }

    public double getSide2() {
        return side2;
    }

    public double getSide3() {
        return side3;
    }

    protected double perimeterOfTriangle(double side1, double side2, double side3) {
        return (side1 + side2 + side3);
    }

    protected double areaOfTriangle(double side1, double side2, double side3){
        double halfPerimeter = perimeterOfTriangle(side1, side2, side3);
        double area = Math.sqrt(halfPerimeter * (halfPerimeter - side1) * (halfPerimeter - side2) * (halfPerimeter - side3));
        return area;
    }
}
