package lesson6;

public class Main {
    public static void main(String[] args) {
        //Product
        Product product = new Product("Greens","N00000000125");
        Product food = new Product("Carrot", "S00000000745", 50);
        Product beverage = new Product("Milk", "S00000000780", 80, "Milk 3,2%", true);

        product.setAvailability(false);
        food.setDescription("Peeled carrot");
        beverage.setPrice(99.99);

        System.out.println(product.getProductName() + " is not available for sale now.");
        System.out.println(food.getDescription() + " with article " + food.getArticle() + " is cost " + food.getPrice());
        System.out.println(beverage.getProductName() + " with cost " + beverage.getPrice() + " is available now");

        //Employee
        Employee employee = new Employee("Taras","Shevchenko","Poet",100);

        double salaryPerMonth = employee.salaryPerMonth(employee.getHourlySalary(),20);
        double salaryPerMonthWithTaxes = employee.salaryPerMonthWithTaxes(employee.getHourlySalary(),20,2.5);

        System.out.println("\nEmployee " + employee.getFirstName() + " " + employee.getLastName() +
                " in position " + employee.getPosition() + " earn " + salaryPerMonth);
        System.out.println("Salary with taxes " + salaryPerMonthWithTaxes);

        employee.setPosition("Main poet");
        employee.setHourlySalary(200);
        double salaryPerMonth1 = employee.salaryPerMonth(employee.getHourlySalary(),20);
        double salaryPerMonthWithTaxes1 = employee.salaryPerMonthWithTaxes(employee.getHourlySalary(),20,2.5);

        System.out.println("New salary for " + employee.getFirstName() + " " + employee.getLastName() +
                " in position " + employee.getPosition() + " is " + salaryPerMonth1);
        System.out.println("Salary with taxes " + salaryPerMonthWithTaxes1);

        //Triangle
        Triangle triangle = new Triangle(3,4,5);

        System.out.println("\nPerimeter of triangle is " +
                triangle.perimeterOfTriangle(triangle.getSide1(), triangle.getSide2(), triangle.getSide3()));

        double areaOfTriangle = triangle.areaOfTriangle(triangle.getSide1(), triangle.getSide2(), triangle.getSide3());
        System.out.println("Area of triangle is " + String.format("%.2f", areaOfTriangle));

        //Credit
        Credit credit1 = new Credit();
        Credit credit2 = new Credit(5000,10,3.5);

        double monthPayment1 = credit1.monthPayment(10000, 5,1);
        System.out.println("\nPayment for credit per month " + String.format("%.2f",monthPayment1));

        double monthPayment2 = credit2.monthPayment(credit2.getAmountOfLoan(), credit2.getLendingPeriod(), credit2.getLendingRate());
        System.out.println("\nIf you got " + credit2.getAmountOfLoan() + " credit for period " + credit2.getLendingPeriod()
                + " month(s), you will pay " + String.format("%.2f", monthPayment2) + " per month.");

        double bankInterest = credit2.bankInterest(credit2.getAmountOfLoan(), credit2.getLendingRate());
        System.out.println("Your total overpayment is " + String.format("%.2f", bankInterest));
    }
}
