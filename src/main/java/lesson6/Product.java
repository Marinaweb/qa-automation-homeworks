package lesson6;

public class Product {
    private String productName;
    private String article;
    private double price;
    private String description;
    private boolean availability;

    public Product(String productName, String article) {
        this.productName = productName;
        this.article = article;
    }

    public Product(String productName, String article, double price) {
        this.productName = productName;
        this.article = article;
        this.price = price;
    }

    public Product(String productName, String article, double price, boolean availability) {
        this.productName = productName;
        this.article = article;
        this.price = price;
        this.availability = availability;
    }

    public Product(String productName, String article, double price, String description, boolean availability) {
        this.productName = productName;
        this.article = article;
        this.price = price;
        this.description = description;
        this.availability = availability;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getArticle() {
        return article;
    }

    public void setArticle(String article) {
        this.article = article;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isAvailability() {
        return availability;
    }

    public void setAvailability(boolean availability) {
        this.availability = availability;
    }
}
