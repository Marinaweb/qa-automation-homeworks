package lesson10;
import java.util.HashMap;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please, enter your sentence");
        String sentence = scanner.nextLine();

        HashMap<Character, Integer> numberOfCharacters = characterCounter(sentence);

         for(Character ch : numberOfCharacters.keySet()) {
             System.out.println(ch + " = " + numberOfCharacters.get(ch));
         }
    }

    public static HashMap<Character, Integer> characterCounter (String sentence) {
        String s = sentence.replaceAll("\\W", "");
        char[] charactersOfSentence = s.toCharArray();

        HashMap<Character, Integer> numberOfEveryCharacter = new HashMap<>();
        Character ch = charactersOfSentence[0];
        numberOfEveryCharacter.put(ch, 0);

        for (int i = 0; i < charactersOfSentence.length; i++) {
            Character ch1 = charactersOfSentence[i];
            if(!ch1.equals(ch)){
                for(Character char1 : numberOfEveryCharacter.keySet()) {
                    if(!char1.equals(ch1)){
                        numberOfEveryCharacter.put(ch1, 0);
                    }
                    break;
                }
            }
        }

       for (int i = 0; i < charactersOfSentence.length; i++) {
            Character ch1 = charactersOfSentence[i];

            for(Character char1 : numberOfEveryCharacter.keySet()) {
                if(ch1.equals(char1)) {
                   Integer counter = numberOfEveryCharacter.get(char1) + 1;
                   numberOfEveryCharacter.put(char1, counter);
                }
            }
       }

        return numberOfEveryCharacter;
    }
}
