package lesson22;

public class QuadraticEquation {
    private double coefficient1;
    private double coefficient2;
    private double coefficient3;

    public QuadraticEquation(double coefficient1, double coefficient2, double coefficient3) {
        this.coefficient1 = coefficient1;
        this.coefficient2 = coefficient2;
        this.coefficient3 = coefficient3;
    }

    public double countDiscriminant(){
        return (Math.pow(coefficient2, 2) - 4 * coefficient1 * coefficient3);
    }

    public int numberOfRoots(double discriminant){
        if(coefficient1 == 0) {
            System.out.println("Перший кофіцієнт не може бути нулем.");
            return 0;
        }

        if(discriminant < 0) {
            return 0;
        } else if(discriminant == 0){
            return 1;
        } else {
            return 2;
        }
    }

    public double countOneRoot(double discriminant){
       return (- coefficient2 + Math.sqrt(discriminant)) / (2 * coefficient1);
    }

    public double[] countTwoRoots(double discriminant){
        double[] roots = new double[2];
        roots[0] = (- coefficient2 + Math.sqrt(discriminant)) / (2 * coefficient1);
        roots[1] = (- coefficient2 - Math.sqrt(discriminant)) / (2 * coefficient1);
        return roots;
    }

    protected void outputToConsole(){
        System.out.println("Рівняння " + coefficient1 + " * x^2 + " + coefficient2 + " * x + " + coefficient3
                + " = 0 має корені:\n");

        double discriminant = countDiscriminant();
        int numberOfRoots = numberOfRoots(discriminant);

        switch (numberOfRoots){
            case 0:
                System.out.println("Рівняння немає коренів.");
                break;
            case 1:
                System.out.println("x1 = " + countOneRoot(discriminant));
                break;
            case 2:
                double[] roots = countTwoRoots(discriminant);
                System.out.println("x1 = " + roots[0]);
                System.out.println("x2 = " + roots[1]);
                break;
        }
    }
    public double getCoefficient1() {
        return coefficient1;
    }

    public double getCoefficient2() {
        return coefficient2;
    }

    public double getCoefficient3() {
        return coefficient3;
    }
}
