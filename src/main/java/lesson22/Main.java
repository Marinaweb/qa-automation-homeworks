package lesson22;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please, enter first coefficient of quadratic equation");
        double coefficient1 = scanner.nextDouble();
        System.out.println("Please, enter second coefficient of quadratic equation");
        double coefficient2 = scanner.nextDouble();
        System.out.println("Please, enter third coefficient of quadratic equation");
        double coefficient3 = scanner.nextDouble();

        QuadraticEquation quadraticEquation = new QuadraticEquation(coefficient1, coefficient2, coefficient3);
        quadraticEquation.outputToConsole();
    }
}
