package lesson19;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;
import static lesson19.Randomizer.*;

public class Main {
    public static void main(String[] args) {
        List<User> users = Stream.generate(() -> new User(generateRandomName(), generateRandomAge(),generateRandomEmail(),selectRandomGender()))
                .limit(20).toList();

        for(User u : users){
            System.out.println(u.toString());
        }

        System.out.println("Results:");

        List<User> listOfUsers = users.stream()
                .limit(20)
                .filter(user -> user.getAge() >= 18)
                .sorted((user1, user2) -> user1.getName().compareTo(user2.getName()) )
                .filter(user -> user.getEmail() != null)
                .sorted((user1, user2) -> user1.getGender().compareTo(user2.getGender()))
                .map(user -> {
                    user.getGender();
                    user.getName();
                    return user;
                }).toList();

        System.out.println(listOfUsers.stream().collect(Collectors.groupingBy(User::getGender,Collectors.mapping(User::getName, toList()))));

    }
}
