package lesson19;

import java.util.Random;

public class Randomizer {
    public static String generateRandomName(){
        int leftLimit = 97; // letter 'a'
        int rightLimit = 122; // letter 'z'
        int targetStringLength = 5;
        Random random = new Random();

        return random.ints(leftLimit, rightLimit + 1)
                .limit(targetStringLength)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();
    }

    public static int generateRandomAge() {
        return new Random().nextInt(110);
    }

    public static String generateRandomEmail(){
        Random random = new Random();
        int i = random.nextInt(10);
        if(i % 2 == 0) {
            return null;
        }

        int leftLimit = 97; // letter 'a'
        int rightLimit = 122; // letter 'z'
        int targetStringLength = 10;
        Random random1 = new Random();

        String firstPart = random1.ints(leftLimit, rightLimit + 1)
                .limit(targetStringLength)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();

        String secondPart = random1.ints(leftLimit, rightLimit + 1)
                .limit(3)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();

        String thirdPart = random1.ints(leftLimit, rightLimit + 1)
                .limit(2)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();

        return firstPart + "@" + secondPart + "." + thirdPart;
    }

    public static String selectRandomGender() {
        Random random = new Random();
        int i = random.nextInt(2);
        if(i % 2 == 0) {
            return "male";
        } else {
            return "female";
        }
    }
}
