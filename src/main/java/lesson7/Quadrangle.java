package lesson7;

public class Quadrangle extends Figure{
    private double side1;
    private double side2;
    private double side3;
    private double side4;
    private int corner1;
    private int corner2;

    public Quadrangle(double side1, double side2) {
        this.side1 = side1;
        this.side2 = side2;
    }

    public Quadrangle(double side1, int corner1) {
        this.side1 = side1;
        this.corner1 = corner1;
    }

    public Quadrangle(double side1, double side2, int corner1) {
        this.side1 = side1;
        this.side2 = side2;
        this.corner1 = corner1;
    }

    public Quadrangle(double side1, double side2, double side3, double side4) {
        this.side1 = side1;
        this.side2 = side2;
        this.side3 = side3;
        this.side4 = side4;
    }

    public Quadrangle(double side1, double side2, double side3, double side4, int corner1, int corner2) {
        this.side1 = side1;
        this.side2 = side2;
        this.side3 = side3;
        this.side4 = side4;
        this.corner1 = corner1;
        this.corner2 = corner2;
    }

    @Override
    public double getPerimeter() {
        return (side1 + side2 + side3 + side4);
    }
    @Override
    public double getSquare() {
        double square = (side1 * side4 * Math.cos(corner1)) / 2 + (side2 * side3 * Math.cos(corner2)) / 2;
        return square;
    }

    protected boolean isQuadrangle(){
        boolean isQuadrangle = false;

        if(side1 > 0 && side2 > 0 && side3 > 0 && side4 > 0 && corner1 > 0 && corner2 > 0) {
            isQuadrangle = true;
        }

        return isQuadrangle;
    }

    public double getSide1() {
        return side1;
    }

    public double getSide2() {
        return side2;
    }

    public double getSide3() {
        return side3;
    }

    public double getSide4() {
        return side4;
    }

    public int getCorner1() {
        return corner1;
    }

    public int getCorner2() {
        return corner2;
    }
}
