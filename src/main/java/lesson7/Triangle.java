package lesson7;

public class Triangle extends Figure {
    private double side1;
    private double side2;
    private double side3;

    public Triangle(double side1) {
        this.side1 = side1;
    }

    public Triangle(double side1, double side2) {
        this.side1 = side1;
        this.side2 = side2;
    }

    public Triangle(double side1, double side2, double side3) {
        this.side1 = side1;
        this.side2 = side2;
        this.side3 = side3;
    }

    protected boolean isTriangle() {
        boolean isTriangle = false;

        if(side1 < (side2 + side3) && side2 < (side1 + side3) && side3 < (side1 + side2) && side1 > 0 && side2 >0 && side3 > 0) {
            isTriangle = true;
        }

        return isTriangle;
    }
    @Override
    public double getPerimeter() {
        return (side1 + side2 + side3);
    }
    @Override
    public double getSquare(){
        double halfPerimeter = getPerimeter();
        double square = Math.sqrt(halfPerimeter * (halfPerimeter - side1) * (halfPerimeter - side2) * (halfPerimeter - side3));
        return square;
    }
    public double getSide1() {
        return side1;
    }

    public double getSide2() {
        return side2;
    }

    public double getSide3() {
        return side3;
    }

}
