package lesson7;

public class EquilateralTriangle extends Triangle{
    public EquilateralTriangle(double side1) {
        super(side1);
    }

    protected boolean isTriangle() {
        boolean isTriangle = false;

        if(super.getSide1() > 0) {
            isTriangle = true;
        }

        return isTriangle;
    }

    @Override
    public double getPerimeter(){
        return super.getSide1() * 3;
    }

    @Override
    public double getSquare(){
        double squareOfEquilateralTriangle = (Math.sqrt(3) / 4) * Math.pow(super.getSide1(), 2);
        return squareOfEquilateralTriangle;
    }
}
