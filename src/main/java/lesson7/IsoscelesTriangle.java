package lesson7;

public class IsoscelesTriangle extends Triangle{
    private double height;
    public IsoscelesTriangle(double isoscelesSide, double base, double height) {
        super(isoscelesSide, base);
        this.height = height;
    }

    public boolean isTriangle() {
        boolean isTriangle = false;

        if(super.getSide1() < (super.getSide1() + super.getSide2()) && super.getSide2() < super.getSide1() * 2 &&
                super.getSide1() > 0 && super.getSide2() >0) {
            isTriangle = true;
        }

        return isTriangle;
    }

    @Override
    public double getPerimeter(){
        return (super.getSide2() + super.getSide1() * 2);
    }
    @Override
    public double getSquare(){
        return (super.getSide2() * height) / 2;
    }

    public double getHeight() {
        return height;
    }
}
