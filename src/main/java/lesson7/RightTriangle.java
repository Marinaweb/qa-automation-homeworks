package lesson7;

public class RightTriangle extends Triangle{
    public RightTriangle(double katet1, double katet2) {
        super(katet1, katet2);
    }

    protected boolean isTriangle() {
        boolean isTriangle = false;

        if(super.getSide1() > 0 && super.getSide2() > 0) {
            isTriangle = true;
        }

        return isTriangle;
    }

    @Override
    public double getPerimeter(){
        double hypotenuse = Math.sqrt(Math.pow(super.getSide1(), 2) + Math.pow(super.getSide2(), 2));
        return (super.getSide1() + super.getSide2() + hypotenuse);
    }
    @Override
    public double getSquare(){
      double squareOfIsoscelesTriangle = (super.getSide1() * super.getSide2()) / 2;
      return squareOfIsoscelesTriangle;
    }
}
