package lesson7;

public class Rectangle extends Quadrangle{
    public Rectangle(double side1, double side2) {
        super(side1, side2);
    }
    @Override
    public double getPerimeter() {
        return (super.getSide1() * 2 + super.getSide2() * 2);
    }
    @Override
    public double getSquare() {
        return super.getSide1() * super.getSide2();
    }
    boolean isRectangle(){
        boolean isRectangle = false;

        if(super.getSide1() > 0 && super.getSide2() > 0) {
            isRectangle = true;
        }

        return isRectangle;
    }
}
