package lesson7;

public class Parallelogram extends Quadrangle{
    public Parallelogram(double side1, double side2, int corner1) {
        super(side1, side2, corner1);
    }

    @Override
    public double getPerimeter(){
        return (super.getSide1() * 2 + super.getSide2() * 2);
    }

    @Override
    public double getSquare(){
        return super.getSide1() * super.getSide2() * Math.sin(super.getCorner1());
    }

    public boolean isParallelogram(){
        boolean isParallelogram = false;

        if(super.getSide1() > 0 && super.getSide2() > 0 && super.getCorner1() > 0) {
            isParallelogram = true;
        }

        return isParallelogram;
    }
}
