package lesson7;

public class Rhombus extends Quadrangle{
    public Rhombus(double side1, int corner1) {
        super(side1, corner1);
    }

    @Override
    public double getPerimeter(){
        return super.getSide1() * 4;
    }

    @Override
    public double getSquare(){
        return Math.pow(super.getSide1(), 2) * Math.sin(super.getCorner1());
    }

    public boolean isRhombus(){
        boolean isRhombus = false;

        if(super.getSide1() > 0 && super.getCorner1() > 0) {
            isRhombus = true;
        }

        return isRhombus;
    }
}
