package lesson7;

public class Main {
    public static void main(String[] args) {
        //Circle
        Circle circle = new Circle(5);
        double perimeterOfCircle = circle.getPerimeter();
        System.out.println("Perimeter of circle with radius " + circle.getRadius() + " is " +
                String.format("%.2f", perimeterOfCircle));

        double squareOfCircle = circle.getSquare();
        System.out.println("Square of circle with radius " + circle.getRadius() + " is " +
                String.format("%.2f", squareOfCircle));

        //Triangle
        Triangle triangle = new Triangle(4, 5, 3.5);
        boolean isTriangle = triangle.isTriangle();

        if(isTriangle) {
            double perimeterOfTriangle = triangle.getPerimeter();
            System.out.println("\nPerimeter of triangle is " + String.format("%.2f", perimeterOfTriangle));

            double squareOfTriangle = triangle.getSquare();
            System.out.println("Square of triangle is " + String.format("%.2f", squareOfTriangle));
        } else {
            System.out.println("It's impossible to create triangle with " + triangle.getSide1() + ", " +
                    triangle.getSide2() + ", " + triangle.getSide3() + " sides.");
        }

        //Right Triangle
        RightTriangle rightTriangle = new RightTriangle(4,5.5);
        boolean isRightTriangle = rightTriangle.isTriangle();

        if(isRightTriangle) {
            double perimeterOfRightTriangle = rightTriangle.getPerimeter();
            System.out.println("\nPerimeter of right triangle with katets " + rightTriangle.getSide1() +
                    " and " + rightTriangle.getSide2() + " is " + String.format("%.2f", perimeterOfRightTriangle));

            double squareOfRightTriangle = rightTriangle.getSquare();
            System.out.println("Square of right triangle with katets " + rightTriangle.getSide1() +
                    " and " + rightTriangle.getSide2() + " is " + String.format("%.2f", squareOfRightTriangle));
        }else {
            System.out.println("It's impossible to create triangle with " + triangle.getSide1() + ", " +
                    triangle.getSide2() + ", " + triangle.getSide3() + " sides.");
        }

        //Isosceles Triangle
        IsoscelesTriangle isoscelesTriangle = new IsoscelesTriangle(5, 4,3);
        boolean isIsoscelesTriangle = isoscelesTriangle.isTriangle();

        if(isIsoscelesTriangle) {
            double perimeterOfIsoscelesTriangle = isoscelesTriangle.getPerimeter();
            System.out.println("\nPerimeter of isosceles triangle with isosceles side " + isoscelesTriangle.getSide1() +
                    " and base " + isoscelesTriangle.getSide2() + " is " + String.format("%.2f", perimeterOfIsoscelesTriangle));

            double squareOfIsoscelesTriangle = isoscelesTriangle.getSquare();
            System.out.println("Square of isosceles triangle with isosceles side " + isoscelesTriangle.getSide1() +
                    " and base " + isoscelesTriangle.getSide2() + " is " + String.format("%.2f", squareOfIsoscelesTriangle));
        } else {
            System.out.println("It's impossible to create triangle with " + isoscelesTriangle.getSide1() + " and " +
                    isoscelesTriangle.getSide2() + " sides.");
        }

        //Equilateral Triangle
        EquilateralTriangle equilateralTriangle = new EquilateralTriangle(5);
        boolean isEquilateralTriangle = equilateralTriangle.isTriangle();

        if(isEquilateralTriangle) {
            double perimeterOfEquilaterialTriangle = equilateralTriangle.getPerimeter();
            System.out.println("\nPerimeter of equilateral triangle with side " + equilateralTriangle.getSide1()
                    + " is " + String.format("%.2f", perimeterOfEquilaterialTriangle));

            double squareOfEquilateralTriangle = equilateralTriangle.getSquare();
            System.out.println("Square of equilateral triangle with side " + equilateralTriangle.getSide1()
                    + " is " + String.format("%.2f", squareOfEquilateralTriangle));
        } else {
            System.out.println("It's impossible to create triangle with " +
                    equilateralTriangle.getSide1() + " sides.");
        }

        //Quadrangle
        Quadrangle quadrangle = new Quadrangle(4, 4.5, 5, 3.8, 30, 45);
        boolean isQuadrangle = quadrangle.isQuadrangle();

        if(isQuadrangle) {
            double perimeterOfQuadrangle = quadrangle.getPerimeter();
            System.out.println("\nPerimeter of quadrangle is " + String.format("%.2f", perimeterOfQuadrangle));

            double squareOfQuadrangle = quadrangle.getSquare();
            System.out.println("Square of quadrangle is " + String.format("%.2f", squareOfQuadrangle));
        } else {
            System.out.println("It's impossible to create quadrangle with such parameters");
        }

        //Square
        Square square = new Square(5.5,90);
        boolean isSquare = square.isSquare();

        if(isSquare) {
            double perimeterOfSquare = square.getPerimeter();
            System.out.println("\nPerimeter of square is " + String.format("%.2f", perimeterOfSquare));

            double squareOfSquare = square.getSquare();
            System.out.println("Square of square is " + String.format("%.2f", squareOfSquare));
        }else {
            System.out.println("It's impossible to create square with such parameters");
        }

        //Rectangle
        Rectangle rectangle = new Rectangle(5,8);
        boolean isRectangle = rectangle.isRectangle();

        if(isRectangle) {
            double perimeterOfRectangle = rectangle.getPerimeter();
            System.out.println("\nPerimeter of rectangle is " + String.format("%.2f", perimeterOfRectangle));

            double squareOfRectangle = rectangle.getSquare();
            System.out.println("Square of rectangle is " + String.format("%.2f", squareOfRectangle));
        } else {
            System.out.println("It's impossible to create square with such parameters");
        }

        //Parallelogram
        Parallelogram parallelogram = new Parallelogram(4,6,45);
        boolean isParallelogram = parallelogram.isParallelogram();

        if(isParallelogram) {
            double perimeterOfParallelogram = parallelogram.getPerimeter();
            System.out.println("\nPerimeter of parallelogram is " + String.format("%.2f", perimeterOfParallelogram));

            double squareOfParallelogram = parallelogram.getSquare();
            System.out.println("Square of parallelogram is " + String.format("%.2f", squareOfParallelogram));
        } else {
            System.out.println("It's impossible to create square with such parameters");
        }

        //Trapeze
        Trapeze trapeze = new Trapeze(4,5,3.5,4.2,2.5);
        boolean isTrapeze = trapeze.isTrapeze();

        if(isTrapeze) {
            double perimeterOfTrapeze = trapeze.getPerimeter();
            System.out.println("\nPerimeter of trapeze is " + String.format("%.2f", perimeterOfTrapeze));

            double squareOfTrapeze = trapeze.getSquare();
            System.out.println("Square of trapeze is " + String.format("%.2f", squareOfTrapeze));
        } else {
            System.out.println("It's impossible to create square with such parameters");
        }

        //Rhombus
        Rhombus rhombus = new Rhombus(5,70);
        boolean isRhombus = rhombus.isRhombus();

        if(isRhombus) {
            double perimeterOfRhombus = rhombus.getPerimeter();
            System.out.println("\nPerirmeter rhombus of  is " + String.format("%.2f", perimeterOfRhombus));

            double squareOfRhombus = rhombus.getSquare();
            System.out.println("Square of rhombus is " + String.format("%.2f", squareOfRhombus));
        } else {
            System.out.println("\nIt's impossible to create rhombus with such parameters");
        }
    }
}
