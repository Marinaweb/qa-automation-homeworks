package lesson7;

public abstract class Figure {
    public abstract double getPerimeter();
    public abstract double getSquare();
}
