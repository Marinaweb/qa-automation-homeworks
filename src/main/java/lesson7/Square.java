package lesson7;

public class Square extends Quadrangle {
    public Square(double side1, int corner1) {
        super(side1, corner1);
    }

    @Override
    public double getPerimeter(){
        return super.getSide1() * 4;
    }

    @Override
    public double getSquare(){
        return Math.pow(super.getSide1(), 2);
    }

    public boolean isSquare(){
        boolean isSquare = false;

        if(super.getSide1() > 0 && super.getCorner1() == 90) {
            isSquare = true;
        }
        return isSquare;
    }
}
