package lesson7;

public class Trapeze extends Quadrangle{
    private double height;

    public Trapeze(double side1, double side2, double side3, double side4, double height) {
        super(side1, side2, side3, side4);
        this.height = height;
    }

    @Override
    public double getPerimeter() {
        return (super.getSide1() + super.getSide2() + super.getSide3() + super.getSide4());
    }
    @Override
    public double getSquare() {
        return (super.getSide1() + super.getSide2()) / 2 * height;
    }
    protected boolean isTrapeze(){
        boolean isTrapeze = false;

        if(super.getSide1() > 0 && super.getSide2() > 0 && super.getSide3() > 0 && super.getSide4() > 0 && height > 0) {
            isTrapeze = true;
        }
        return isTrapeze;
    }

    public double getHeight() {
        return height;
    }
}
