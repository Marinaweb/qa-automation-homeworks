package lesson7;

public class Circle extends Figure{
    private double radius;

    public Circle(double radius) {
        this.radius = radius;
    }
    public double getRadius() {
        return radius;
    }
    @Override
    public double getPerimeter(){
       return 2 * Math.PI * radius;
    }
    @Override
    public double getSquare(){
       return Math.PI * Math.pow(radius, 2);
    }
}

