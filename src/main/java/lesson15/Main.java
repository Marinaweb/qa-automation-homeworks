package lesson15;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.support.ui.FluentWait;
import java.time.Duration;
import java.util.List;
import java.util.NoSuchElementException;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        String browserName = System.getenv("browser");
        RemoteWebDriver browser = getDriver(browserName);
        browser.get("https://ithillel.ua/");
        browser.manage().window().maximize();
        browser.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));

        WebElement linkToProgramming = browser.findElement(By.cssSelector("a.block-course-cats_link[href*='programming']"));
        linkToProgramming.click();

        WebElement linkToFrontEndBasicCourse = browser.findElement(By.cssSelector("a.block-profession_link[href*='front-end-basic']"));
        linkToFrontEndBasicCourse.click();

        FluentWait<RemoteWebDriver> wait = new FluentWait<>(browser)
                .pollingEvery(Duration.ofMillis(100))
                .withTimeout(Duration.ofSeconds(15))
                .ignoring(NoSuchElementException.class);


        WebElement coachesSection = wait.until((driver) -> {
            driver.executeScript("scrollBy(0,200)");
            return driver.findElement(By.id("coachesSection"));
        });

       List<WebElement> teachers = coachesSection.findElements(By.className("coach-card_name"));
       teachers.stream().map(WebElement::getText).forEach(System.out::println);

        browser.quit();
    }

    static RemoteWebDriver getDriver(String driverName){
        switch(driverName) {
            case "chrome":
                return new ChromeDriver();
            case "firefox":
                return new FirefoxDriver();
            case "edge":
                return new EdgeDriver();
            case "safari":
                return new SafariDriver();
            default:
                throw new IllegalArgumentException("No such browser");
        }
    }
}
