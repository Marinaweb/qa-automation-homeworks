package lesson9;

public enum Size {
    SMALL("S", 48,66),
    MIDDLE("M", 53, 68),
    LARGE("L", 57, 71),
    X_LARGE("XL",61, 73),
    XX_LARGE("2XL", 65, 75),
    XXX_LARGE("3XL", 72, 77),
    XXXX_LARGE("4XL", 75, 77),
    XXXXX_LARGE("5XL", 80, 79);
    private String shortName;
    private int width;
    private int length;

    Size(String shortName, int width, int length) {
        this.shortName = shortName;
        this.width = width;
        this.length = length;
    }

    public String getShortName() {
        return shortName;
    }

    public int getWidth() {
        return width;
    }

    public int getLength() {
        return length;
    }

    @Override
    public String toString() {
        if((getShortName().length()) == 1) {
            return getShortName() + "       " + getWidth() + "     " + getLength();
        } else if((getShortName().length()) == 2){
            return getShortName() + "      " + getWidth() + "     " + getLength();
        }
      return getShortName() + "     " + getWidth() + "     " + getLength();
    }
}
