package lesson9;

public class Main {
    public static void main(String[] args) {
        System.out.println("Table of sizes for T-Shorts: \nSize  Width  Length");

        for(Size s: Size.values()) {
            System.out.println(s.toString());
        }
    }
}
