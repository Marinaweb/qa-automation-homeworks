package lesson17;

public interface Constants {
    String LINK_TESTING_COURSES_PAGE = "https://ithillel.ua/courses/testing";
    String LINK_PROGRAMMING = "https://ithillel.ua/courses/programming";
    String LINK_TESTING = "https://ithillel.ua/courses/testing";
    String LINK_MANAGEMENT = "https://ithillel.ua/courses/management";
    String LINK_BUSINESS_TRAINING = "https://ithillel.ua/courses/business-training";
    String LINK_MARKETING = "https://ithillel.ua/courses/marketing";
    String LINK_DESIGN = "https://ithillel.ua/courses/design";
    String LINK_TEENAGERS_12_17_YEARS_OLD = "https://ithillel.ua/courses/kids";
    String LINK_CHILDREN_7_11_YEARS_OLD = "https://ithillel.ua/courses/kids-7-11";
}
