package lesson17;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class BlockOpportunityItem {
    public static final String TITLE_OPPORTUNITY_ITEM = "opportunity-item_title";
    @FindBy(className = TITLE_OPPORTUNITY_ITEM)
    private WebElement title;

    public BlockOpportunityItem(WebElement title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "BlockOpportunityItem{" +
                "title=" + title.getText() +
                "}\n";
    }
}
