package lesson17;

import java.util.List;

public interface Courses {
    void openPage();
    List<BlockProfessionItem> getCourses();
    List<BlockProfessionItem> getAdditionalCourses();
    List<BlockOpportunityItem> getOportunities();
    void goToCategory(String category);
}
