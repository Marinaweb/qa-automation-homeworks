package lesson17;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import java.util.ArrayList;
import java.util.List;
import static lesson17.Constants.*;

public class TestingCoursesPage implements Courses {

    private final WebDriver driver;
    @FindBy(css = "*.block-profession_list")
    private List<WebElement> blockProfessionItemList;

    @FindBy(css = "*.opportunities")
    private WebElement blockOpportunityItem;

    public TestingCoursesPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @Override
    public void openPage() {
        driver.get(LINK_TESTING_COURSES_PAGE);
    }

    @Override
    public List<BlockProfessionItem> getCourses() {
        List<BlockProfessionItem> listProfessionItems = new ArrayList<>();

        for (int i = 0; i < (blockProfessionItemList.size() - 1); i++) {
            WebElement item = blockProfessionItemList.get(i);
            item.findElements(By.cssSelector("*.block-profession_item"))
                    .stream()
                    .map(iter -> {
                        return listProfessionItems.add(PageFactory.initElements(iter, BlockProfessionItem.class));
                    }).toList();

        }

        return listProfessionItems;
    }

    @Override
    public List<BlockProfessionItem> getAdditionalCourses() {
        List<BlockProfessionItem> listAdditionalItems = new ArrayList<>();

        WebElement item = blockProfessionItemList.get((blockProfessionItemList.size() - 1));
        item.findElements(By.cssSelector("*.block-profession_item"))
                .stream()
                .map(iter -> {
                    return listAdditionalItems.add(PageFactory.initElements(iter, BlockProfessionItem.class));
                }).toList();

        return listAdditionalItems;
    }

    @Override
    public List<BlockOpportunityItem> getOportunities() {
        return blockOpportunityItem.findElements(By.cssSelector("*.opportunities_item"))
                .stream()
                .map(item -> PageFactory.initElements(item, BlockOpportunityItem.class))
                .toList();
    }

    @Override
    public void goToCategory(String category) {

        switch (category) {
            case "Програмування":
                driver.get(LINK_PROGRAMMING);
                break;
            case "Тестування":
                driver.get(LINK_TESTING);
                break;
            case "Менеджмент":
                driver.get(LINK_MANAGEMENT);
                break;
            case "Бізнес навчання":
                driver.get(LINK_BUSINESS_TRAINING);
                break;
            case "Маркетинг":
                driver.get(LINK_MARKETING);
                break;
            case "Дизайн":
                driver.get(LINK_DESIGN);
                break;
            case "Підліткові 12-17 років":
                driver.get(LINK_TEENAGERS_12_17_YEARS_OLD);
                break;
            case "Дитячі 7-11 років":
                driver.get(LINK_CHILDREN_7_11_YEARS_OLD);
                break;
            default:
                throw new IllegalArgumentException("No such page");
        }
    }
}




