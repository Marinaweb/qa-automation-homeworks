package lesson17;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class BlockProfessionItem {
   @FindBy(className = "profession-bar_title")
    private WebElement title;
    @FindBy(className = "profession-subtitle_grade")
    private WebElement subtitle;
    @FindBy(className = "profession-bar_descr")
    private WebElement description;

   public BlockProfessionItem(WebElement title, WebElement subtitle, WebElement description) {
        this.title = title;
        this.subtitle = subtitle;
        this.description = description;
    }

   public String getTitle() {
        return title.getText();
    }

    public String getSubtitle() {
        return subtitle.getText();
    }

    public String getDescription() {
        return description.getText();
    }

    @Override
    public String toString() {
        return "BlockProfessionItem{" +
                "title=" + title.getText() +
                ", subtitle=" + subtitle.getText() +
                ", description=" + description.getText() +
                "}\n";
    }
}
