package lesson8;

public interface Swimable {
    public void swim();
    public default boolean isPredator(){
        return false;
    }
}
