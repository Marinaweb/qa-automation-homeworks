package lesson8;

public abstract class Pet implements Runnable{
    private boolean isDangerousForPeople;
    private String petFood;
    private int numberOfFeedsPerDay;
    private boolean isNeedToWalk;

    public Pet(String petFood, int numberOfFeedsPerDay, boolean isNeedToWalk, boolean isDangerousForPeople) {
        this.isDangerousForPeople = isDangerousForPeople;
        this.petFood = petFood;
        this.numberOfFeedsPerDay = numberOfFeedsPerDay;
        this.isNeedToWalk = isNeedToWalk;
    }

    public abstract boolean isVaccinated();
    @Override
    public void run(){
        if(isNeedToWalk){
            System.out.println("Pet is running on the walk.");
        }else {
            System.out.println("The pet is running during a day at home.");
        }
    }
    @Override
    public boolean isPet(){
        boolean isPet = false;
        if(!isDangerousForPeople){
            isPet = true;
        }
        return isPet;
    }
    public String getPetFood() {
        return petFood;
    }

    public int getNumberOfFeedsPerDay() {
        return numberOfFeedsPerDay;
    }

    public boolean isNeedToWalk() {
        return isNeedToWalk;
    }

    public boolean isDangerousForPeople() {
        return isDangerousForPeople;
    }
}
