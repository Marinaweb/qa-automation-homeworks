package lesson8;

public class Stork extends Bird implements Runnable{
    public Stork(boolean isWings, boolean isBigWeight, String food) {
        super(isWings, isBigWeight, food);
    }

    @Override
    public boolean canFly(){
        boolean canFly = false;
        if(isWings()){
            System.out.println("Stork can fly");
            canFly = true;
        }
        return canFly;
    }

    @Override
    public void fly(){
        System.out.println("Stork can fly for long distance.");
    }

    @Override
    public void run(){
        System.out.println("Stork can run");
    }

    @Override
    public boolean isPet(){
        if((super.getFood()).equalsIgnoreCase("snake") ||(super.getFood()).equalsIgnoreCase("fish")){
            System.out.println("Stork is predator");
        }
        return false;
    }
}
