package lesson8;

public class Plan extends Transport implements Flyable{
    public Plan(double speed, String engine, boolean isTouristTransport, int numberOfSeats) {
        super(speed, engine, isTouristTransport, numberOfSeats);
    }

    @Override
    public boolean isPublicTransport(){
        boolean isPublicTransport = false;
        if(!isTouristTransport() && numberOfPassangerSeats() > 0) {
            isPublicTransport = true;
        }

        return isPublicTransport;
    }

    @Override
    public void fly(){
        System.out.println("Plan can fly with speed" + super.getSpeed());
    }
}
