package lesson8;

public class KiwiBird extends Bird implements Runnable{
    public KiwiBird(boolean isWings, boolean isBigWeight, String food) {
        super(isWings, isBigWeight, food);
    }

    @Override
    public boolean canFly(){
        boolean canFly = false;
        if(isWings()){
            canFly = true;
        }
        return canFly;
    }

    @Override
    public void fly(){
        System.out.println("Kiwi is not flying");
    }

    @Override
    public void run(){
        System.out.println("Kiwi can run.");
    }

    @Override
    public boolean isPet(){
        System.out.println("Kiwi is not a pet.");
        return false;
    }
}
