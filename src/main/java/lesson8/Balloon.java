package lesson8;

public class Balloon extends Transport implements Flyable {
    public Balloon(double speed, String engine, boolean isTouristTransport, int numberOfSeats) {
        super(speed, engine, isTouristTransport, numberOfSeats);
    }

    @Override
    public boolean isPublicTransport(){
        System.out.println("Balloon is tourists transport only.");
        return false;
    }

    @Override
    public void fly(){
        System.out.println("Balloon can fly with speen " + super.getSpeed() + " with engeen " + super.getEngine());
    }
}
