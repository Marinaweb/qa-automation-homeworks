package lesson8;

public abstract class Transport {
    private double speed;
    private String engine;
    private boolean isTouristTransport;
    private int numberOfPassangerSeats;

    public Transport(double speed, String engine, boolean isTouristTransport, int numberOfSeats) {
        this.speed = speed;
        this.engine = engine;
        this.isTouristTransport = isTouristTransport;
        this.numberOfPassangerSeats = numberOfSeats;
    }

    public abstract boolean isPublicTransport();

    public double getSpeed() {
        return speed;
    }

    public String getEngine() {
        return engine;
    }

    public boolean isTouristTransport() {
        return isTouristTransport;
    }

    public int numberOfPassangerSeats() {
        return numberOfPassangerSeats;
    }
}
