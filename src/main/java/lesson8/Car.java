package lesson8;

public class Car extends Transport implements Movable{
    public Car(double speed, String engine, boolean isTouristTransport, int numberOfSeats) {
        super(speed, engine, isTouristTransport, numberOfSeats);
    }

    @Override
    public boolean isPublicTransport(){
       boolean isPublicTransport = false;
       if(super.numberOfPassangerSeats() >= 6){
           System.out.println("Blabla car is available for the car");
           isPublicTransport = true;
       }
        return isPublicTransport;
    }

    @Override
    public void move(){

        if (super.numberOfPassangerSeats() <=15) {
            System.out.println("Bus is moving quickly.");
        } else {
            System.out.println("Bus is moving slowly.");
        }
    }
}
