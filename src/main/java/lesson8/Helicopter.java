package lesson8;

public class Helicopter extends Transport implements Flyable {
    public Helicopter(double speed, String engine, boolean isTouristTransport, int numberOfSeats) {
        super(speed, engine, isTouristTransport, numberOfSeats);
    }

    @Override
    public boolean isPublicTransport(){
        System.out.println("Helicopter is a tourists or private transport");
        return false;
    }

    @Override
    public void fly(){
        System.out.println("Helicopter is flying with speed " + super.getSpeed());
    }
}
