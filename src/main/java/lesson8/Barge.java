package lesson8;

public class Barge extends Transport implements Swimable{
    public Barge(double speed, String engine, boolean isTouristTransport, int numberOfSeats) {
        super(speed, engine, isTouristTransport, numberOfSeats);
    }

    @Override
    public boolean isPublicTransport(){
        return false;
    }

    @Override
    public void swim(){
        System.out.println("The barge is delivering cargo");
    }
}
