package lesson8;

public class Catfish extends Fish{
    public Catfish(String name, String areaOfLiving, String food, boolean isLimnetic) {
        super(name, areaOfLiving, food, isLimnetic);
    }
    @Override
    public void swim(){
        System.out.println("This Catfish can swim");
    }

    @Override
    public boolean isPredator() {
        boolean isPredator = false;
        if(getFood().equalsIgnoreCase("fish")){
            isPredator = true;
        }
        return isPredator;
    }
}
