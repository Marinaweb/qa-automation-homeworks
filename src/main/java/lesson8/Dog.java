package lesson8;

public class Dog extends Pet{
    private boolean isDogCollar;
    private boolean isDogMuzzle;
    private double age;

    public Dog(String petFood, int numberOfFeedsPerDay, boolean isNeedToWalk, boolean isDangerousForPeople,
               boolean isDogCollar, boolean isDogMuzzle, double age) {
        super(petFood, numberOfFeedsPerDay, isNeedToWalk, isDangerousForPeople);
        this.isDogCollar = isDogCollar;
        this.isDogMuzzle = isDogMuzzle;
        this.age = age;
    }

    @Override
    public void run(){
        if(isDogCollar && isDogMuzzle){
            System.out.println("Dog is ready to go to the walk");
        } else {
            System.out.println("Dog can run at home only");
        }
    }

    @Override
    public boolean isPet(){
        boolean isPet = false;
        if(!isDangerousForPeople() && isDogCollar){
            isPet = true;
        }
        return isPet;
    }

    @Override
    public boolean isVaccinated(){
        boolean isVaccinated = false;
        if(age >= 0.5) {
            System.out.println("Your dog is vaccinated");
            isVaccinated = true;
        }
        return isVaccinated;
    }
    public boolean isDogCollar() {
        return isDogCollar;
    }

    public boolean isDogMuzzle() {
        return isDogMuzzle;
    }
}
