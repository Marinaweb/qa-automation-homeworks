package lesson8;

public class Boat extends Transport implements Swimable{
    public Boat(double speed, String engine, boolean isTouristTransport, int numberOfSeats) {
        super(speed, engine, isTouristTransport, numberOfSeats);
    }

    @Override
    public boolean isPublicTransport(){
        boolean isPublicTransport = false;
        if(super.numberOfPassangerSeats() >= 2){
            System.out.println("The boat can be a public transport.");
            isPublicTransport = true;
        }
        return isPublicTransport;
    }
    @Override
    public void swim(){
        System.out.println("Tha boat can swim");
    }
}
