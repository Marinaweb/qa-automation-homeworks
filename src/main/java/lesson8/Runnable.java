package lesson8;

public interface Runnable {
    public void run();
    public boolean isPet();
}
