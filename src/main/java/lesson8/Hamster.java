package lesson8;

public class Hamster extends Pet {
    private boolean isCage;
    private boolean isWheel;

    public Hamster(String petFood, int numberOfFeedsPerDay, boolean isNeedToWalk, boolean isDangerousForPeople,
                   boolean isCage, boolean isWheel) {
        super(petFood, numberOfFeedsPerDay, isNeedToWalk, isDangerousForPeople);
        this.isCage = isCage;
        this.isWheel = isWheel;
    }

    @Override
    public void run(){
        if(isWheel){
            System.out.println("Hamster is running in the wheel.");
        }else {
            System.out.println("Hamster is running in cage.");
        }
    }

    @Override
    public boolean isPet(){
        boolean isPet = false;
        if(super.isDangerousForPeople() && isCage){
            isPet = true;
        }
        return isPet;
    }

    @Override
    public boolean isVaccinated(){
        return false;
    }

    public boolean isCage() {
        return isCage;
    }

    public boolean isWheel() {
        return isWheel;
    }
}
