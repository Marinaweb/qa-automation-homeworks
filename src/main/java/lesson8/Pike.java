package lesson8;

public class Pike extends Fish{
    public Pike(String name, String areaOfLiving, String food, boolean isLimnetic) {
        super(name, areaOfLiving, food, isLimnetic);
    }
    @Override
    public void swim(){
        System.out.println("Pike can swim");
    }

}
