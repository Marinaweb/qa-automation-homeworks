package lesson8;

public class Bus extends Transport implements Movable{
    public Bus(double speed, String engine, boolean isTouristTransport, int numberOfSeats) {
        super(speed, engine, isTouristTransport, numberOfSeats);
    }

    @Override
    public boolean isPublicTransport(){
        boolean isPublicTransport = false;
        if(numberOfPassangerSeats() >= 1){
            isPublicTransport = true;
        }
        return isPublicTransport;
    }

    @Override
    public void move(){
        if (getEngine().equalsIgnoreCase("reactive")) {
            System.out.println("Car is moving quickly.");
        } else {
            System.out.println("Car is moving slowly.");
        }
    }
}
