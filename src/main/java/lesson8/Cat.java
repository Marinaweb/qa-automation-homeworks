package lesson8;

public class Cat extends Pet{
    private boolean isRegistered;

    public Cat(String petFood, int numberOfFeedsPerDay, boolean isNeedToWalk, boolean isDangerousForPeople) {
        super(petFood, numberOfFeedsPerDay, isNeedToWalk, isDangerousForPeople);
    }

    @Override
    public void run(){
            System.out.println("The pet is running only for feed.");
    }

    @Override
    public boolean isVaccinated(){
        boolean isVaccinated = false;
        if(isRegistered) {
            System.out.println("Your cat is vaccinated");
            isVaccinated = true;
        }
        return isVaccinated;
    }

    @Override
    public boolean isPet(){
        boolean isPet = false;
        if(isRegistered){
            System.out.println("You could bring your cat to home");
        }
        return isPet;
    }

    public boolean isRegistered() {
        return isRegistered;
    }
}
