package lesson8;

public class Tractor extends Transport implements Movable{
    private String workingMode;

    public Tractor(double speed, String engine, boolean isTouristTransport, int numberOfSeats, String workingMode) {
        super(speed, engine, isTouristTransport, numberOfSeats);
        this.workingMode = workingMode;
    }

    @Override
    public boolean isPublicTransport(){
        return false;
    }

    @Override
    public void move(){
       if(workingMode.equalsIgnoreCase("plow the field")){
           System.out.println("Tractor is plowing the field");
       } else if(workingMode.equalsIgnoreCase("harvest")){
           System.out.println("Tractor is collecting harvert.");
       }
    }

    public String getWorkingMode() {
        return workingMode;
    }
}
