package lesson8;

public class Eagle extends Bird{
    public Eagle(boolean isWings, boolean isBigWeight, String food) {
        super(isWings, isBigWeight, food);
    }

    @Override
    public boolean canFly(){
        boolean canFly = false;
        if(isWings()){
            canFly = true;
        }
        return canFly;
    }

    @Override
    public void fly(){
        System.out.println("Eagle can fly for long distance.");
    }

}
