package lesson8;

public abstract class Bird implements Flyable {
    private boolean isWings;
    private boolean isBigWeight;
    private String food;

    public Bird(boolean isWings, boolean isBigWeight, String food) {
        this.isWings = isWings;
        this.isBigWeight = isBigWeight;
        this.food = food;
    }

    public abstract boolean canFly();

    public boolean isWings() {
        return isWings;
    }

    public boolean isBigWeight() {
        return isBigWeight;
    }

    public String getFood() {
        return food;
    }
}
