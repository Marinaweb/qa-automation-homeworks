package lesson8;

public class Kater extends Transport implements Swimable{
    public Kater(double speed, String engine, boolean isTouristTransport, int numberOfSeats) {
        super(speed, engine, isTouristTransport, numberOfSeats);
    }

    @Override
    public boolean isPublicTransport(){
        boolean isPublicTransport = false;
        if(super.numberOfPassangerSeats() >= 2){
            System.out.println("The boat can be user as public pransport");
            isPublicTransport = true;
        }
        return isPublicTransport;
    }

    @Override
    public void swim(){
        System.out.println("Kater is swim");
    }
}
