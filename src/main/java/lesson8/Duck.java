package lesson8;

public class Duck extends Bird implements Swimable, Runnable{
    private boolean isWingsNotCut;

    public Duck(boolean isWings, boolean isBigWeight, String food, boolean isWingsNotCut) {
        super(isWings, isBigWeight, food);
        this.isWingsNotCut = isWingsNotCut;
    }

    @Override
    public boolean canFly(){
        boolean canFly = false;
        if(isWingsNotCut && !isBigWeight()){
            canFly = true;
        }
        return canFly;
    }

    @Override
    public void fly(){
        if(canFly()){
            System.out.println("The duck can fly");
        } else{
            System.out.println("Duck can't fly");
        }
    }

    @Override
    public void swim(){
        System.out.println("Duck can swim");
    }

    @Override
    public boolean isPredator(){
        boolean isPredator = false;
        if(getFood().equalsIgnoreCase("fish")){
            isPredator = true;
        }

        return isPredator;
    }

    @Override
    public void run(){
        System.out.println("Duck can run");
    }

    @Override
    public boolean isPet(){
        boolean isPet = false;
        if(!isWingsNotCut) {
            return isPet = true;
        }
        return isPet;
    }
    public boolean isWingsNotCut() {
        return isWingsNotCut;
    }
}
