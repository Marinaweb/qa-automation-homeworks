package lesson8;

public class Penguin extends Bird implements Swimable, Runnable{
    public Penguin(boolean isWings, boolean isBigWeight, String food) {
        super(isWings, isBigWeight, food);
    }

    @Override
    public boolean canFly(){
        boolean canFly = false;
        if(isWings() && !isBigWeight()){
            canFly = true;
        }
        return canFly;
    }

    @Override
    public void fly(){
        if(canFly()) {
            System.out.println("The bird can fly.");
        } else {
            System.out.println("This bird cannot fly");
        }
    }

    @Override
    public void swim(){
        System.out.println("Penguin can swim");
    }

    @Override
    public boolean isPredator(){
        boolean isPredator = false;
        if(getFood().equalsIgnoreCase("fish")){
            isPredator = true;
        }
        return isPredator;
    }

    @Override
    public void run(){
        System.out.println("Penguin can run");
    }

    @Override
    public boolean isPet(){
        System.out.println("Penguin can't be a pet.");
        return false;
    }
}
