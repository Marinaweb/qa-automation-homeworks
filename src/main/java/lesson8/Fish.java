package lesson8;

public abstract class Fish implements Swimable {
    private String name;
    private String areaOfLiving;
    private String food;
    private boolean isLimnetic;

    public Fish(String name, String areaOfLiving, String food, boolean isLimnetic) {
        this.name = name;
        this.areaOfLiving = areaOfLiving;
        this.food = food;
        this.isLimnetic = isLimnetic;
    }

    public void swim(){
        System.out.println("This fish can swim");
    }

    @Override
    public boolean isPredator() {
        boolean isPredator = false;
        if(food.equalsIgnoreCase("fish")){
            isPredator = true;
        }
        return isPredator;
    }

    public String getName() {
        return name;
    }

    public String getAreaOfLiving() {
        return areaOfLiving;
    }

    public String getFood() {
        return food;
    }

    public boolean isLimnetic() {
        return isLimnetic;
    }
}
