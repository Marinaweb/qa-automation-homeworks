package lesson8;

public class Pigeon extends Bird implements Runnable{
    public Pigeon(boolean isWings, boolean isBigWeight, String food) {
        super(isWings, isBigWeight, food);
    }

    @Override
    public boolean canFly(){
        boolean canFly = false;
        if(!isBigWeight()){
            System.out.println("Pigeon can fly");
            canFly = true;
        }
        return canFly;
    }

    @Override
    public void fly(){
        System.out.println("Pigen can fly.");
    }

    @Override
    public void run(){
        System.out.println("Pigeon can run fot a long distance");
    }

    @Override
    public boolean isPet(){
        boolean isPet = false;
        if(isBigWeight()) {
            isPet = true;
        }

        return isPet;
    }
}
