package lesson8;

public class Ostrich extends Bird implements Runnable{
    public Ostrich(boolean isWings, boolean isBigWeight, String food) {
        super(isWings, isBigWeight, food);
    }

    @Override
    public boolean canFly(){
        boolean canFly = false;
        if(!isBigWeight()){
            canFly = true;
        }
        return canFly;
    }

    @Override
    public void fly(){
        System.out.println("Ostrich can't fly");
    }

    @Override
    public void run(){
        System.out.println("Ostrich can run");
    }

    @Override
    public boolean isPet(){
        System.out.println("Ostrich is too dangerous for being a pet.");
        return false;
    }
}
